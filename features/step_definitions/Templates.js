/* jslint node: true */
'use strict';
var apickli = require('apickli');
var factory = require('./factory.js');
var config = require('../../config/config.json');
var Contenttype = "application/json";
var AuthorizationValue;
var SPSUserValue;

var prettyJson = require('prettyjson');


var getTemplateData = function( apickli,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;
		
		var url = apickli.domain + pathSuffix;
		apickli.addRequestHeader('Authorization',apickli.scenarioVariables.AuthorizationValue);
		console.log(apickli.scenarioVariables.AuthorizationValue);
		apickli.addRequestHeader('SPS-User',apickli.scenarioVariables.SPSUserValue);
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");		
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

module.exports = function () { 

		this.Given(/^I have valid Templates request data$/, function (callback) 
		{		
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.SPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.Authorization);			
		callback();
		
	});
	
	this.When(/^I get data for Templates (.*)$/, function (getTemplateAction,callback) {
	      	getTemplateData(this.apickli,getTemplateAction,callback);
	});	
	
	this.Given(/^I enter invalid SPS-User request data$/, function (callback) {	
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.InvalidSPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue",this.apickli.scenarioVariables.Authorization);
	
		callback();
		
	});
	
	this.Given(/^I enter invalid Authorization request data$/, function (callback) {		
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.SPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.InvalidAuthorization);	
		callback();
		
	});
	
	
	
};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
