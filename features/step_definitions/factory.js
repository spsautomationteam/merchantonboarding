var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.bankcard.basepath;
var defaultDomain = config.bankcard.domain;

console.log('bankcard api: [' +defaultDomain + ', ' + defaultBasePath+ ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;
	
	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;
