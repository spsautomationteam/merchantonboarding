/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

var Contenttype = "application/json";

var prettyJson = require('prettyjson');

var stepContext = {};

var getChargeList = function( apickli, typeOfTransaction, callback ) {
		var pathSuffix = "/charges?type=" + typeOfTransaction;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var verifyReference = function( apickli, callback ) {
		var Reference = apickli.scenarioVariables.Reference;		
		var assertion = apickli.assertResponseBodyContainsExpression(Reference); 
          
        if (assertion.success) {
            callback();
        } else {
            callback(prettyPrintJson(assertion));
        }  
				
		
};

module.exports = function () { 
	
	this.When(/^I get a list of existing (.*) transaction$/, function (typeOfTransaction, callback) {
      	
		getChargeList(this.apickli, typeOfTransaction, callback);

	});
	
	
	
	this.Then(/^response body path should contain above performed transaction reference$/, function (callback) {

		verifyReference(this.apickli, callback);		
		
	});
	
};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
