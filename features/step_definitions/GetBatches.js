/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";


var getBatchList = function(apickli,callback ) {
		var pathSuffix = "/batches";
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';
		
		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				
			}
        	callback();
		});
});
};

var verifyReference = function( apickli, callback ) {
		var Reference = apickli.scenarioVariables.Reference;		
		var assertion = apickli.assertResponseBodyContainsExpression(Reference); 
        if (assertion.success) {
            callback();
        } else {
            callback(prettyPrintJson(assertion));
        }  
				
		
};

var getBatch = function( apickli, Reference, callback ) {
		var pathSuffix = "/batches/" + Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getBatchTransaction = function( apickli, Reference, callback ) {
		var pathSuffix = "/batches/" + Reference+"/transactions";
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


module.exports = function () { 
	
	this.When(/^I get List of Existing Batches$/, function (callback) {
       
		getBatchList(this.apickli,callback);
	});
	
	this.When(/^I get that same existing batch detail$/, function (callback) {
       
		var Reference = this.apickli.scenarioVariables.Reference;
		getBatch(this.apickli,Reference,callback);
	});
	
	this.When(/I get List of Transactions for an Existing Batch$/, function (callback) {
       
		var Reference = this.apickli.scenarioVariables.Reference;
		getBatchTransaction(this.apickli,Reference,callback);
	});
	
	
	this.Then(/^response body path should contain above settled batch reference$/, function (callback) {
		//var Reference = this.apickli.scenarioVariables.Reference;
		//console.log(Reference);
		verifyReference(this.apickli, callback);		
		
	});

	};
