/* jslint node: true */
'use strict';

//var factory = require('./factory.js');
var apickli = require('apickli');
var config = require('../../config/config.json');
var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createToken = function( apickli, cardNo,ExpYear,callback ) {

		var defaultBasePath =  "/token/v1";
		//var defaultDomain1 = "https://api-qa.sagepayments.com";
	
		var defaultDomain ="api-qa.sagepayments.com";	
		

		var pathSuffix = "/tokens";
		var a1= new this.apickli.Apickli('https', defaultDomain+defaultBasePath);
	
		var url = a1.domain + pathSuffix;
		

		//var url = defaultDomain1 +"/token/v1"+ pathSuffix;
		console.log(url);
		
		var body = {"cardData":{"number":cardNo,"expiration":ExpYear}};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;

		var hmac = hmacTools.hmac( clientSecret, "POST", url, body,'apickli.scenarioVariables.merchantId' , nonce, timestamp);    	
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.vaultResponse.data', 'GUID');
			//var GUID = apickli.scenarioVariables.GUID;			
        	callback();
		});
};



module.exports = function () { 

	this.When(/^I Create a token for (.*) credit card number and (.*) expirtation date$/, function (cardNo,ExpYear,callback) {
		createToken(this.apickli,cardNo,ExpYear, callback);
	});
	
		
		
};

	

