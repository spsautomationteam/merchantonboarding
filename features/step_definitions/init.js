/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var authorization = config.bankcard.authorization;
var spsUser = config.bankcard.SPSUser;
 

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        // this.apickli.storeValueInScenarioScope("clientId", "Uvz7ahhlaer3zhOp25BGmAzSQVEbsJNw");
 
		this.apickli.storeValueInScenarioScope("spsUser", spsUser);
		this.apickli.storeValueInScenarioScope("authorization", authorization);

        callback();
    });
};

