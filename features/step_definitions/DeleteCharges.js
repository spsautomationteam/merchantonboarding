/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createTx = function( apickli,TxType, callback ) {
		var pathSuffix = "/charges?type="+TxType;
		var url = apickli.domain + pathSuffix;
		var body = { ECommerce: { Amounts: { Total: 1.0 },authorizationCode: 111222, CardData: { Expiration: 1216, Number: 4111111111111111 } } }
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
			var Reference = apickli.scenarioVariables.Reference;
			
        	callback();     
		});
};


var DeleteTx = function( apickli,pathSuffix,Reference, callback ) {
		
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "DELETE", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.delete(pathSuffix, callback);
		
};

module.exports = function () { 

	this.Given(/^I perform a (.*) transaction$/, function (TxType,callback) {
		createTx(this.apickli,TxType, callback);
	});
	
	this.When(/^I Delete the respective transaction$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		var pathSuffix = "/charges/" + Reference;
		DeleteTx(this.apickli,pathSuffix,Reference,callback);
	});
	
	this.When(/^Again try to delete the same transaction$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		var pathSuffix = "/charges/" + Reference;
		DeleteTx(this.apickli,pathSuffix,Reference,callback);
	});

	};
