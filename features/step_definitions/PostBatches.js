/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createSale = function( apickli, callback ) {
		var pathSuffix = "/charges?type=Sale";
		var url = apickli.domain + pathSuffix;
		var body = { ECommerce: { Amounts: { Total: 1.0 }, CardData: { Expiration: 1216, Number: 4111111111111111 } } }
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


var batchSettle = function(apickli,callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = {"settlementType": "Bankcard"};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
			
			}
        	callback();
		});
};


module.exports = function () { 

	this.Given(/^I create a sale transaction$/, function (callback) {
		createSale(this.apickli, callback);
	});
	
	this.When(/^I settle all current set of transaction$/, function (callback) {
       
		batchSettle(this.apickli,callback);
	});

	};
