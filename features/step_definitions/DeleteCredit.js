/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";


var DeleteTx = function( apickli,pathSuffix,Reference, callback ) {
		
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "DELETE", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.delete(pathSuffix, callback);
		
};
module.exports = function () { 
	
	this.When(/^I Delete the credit transaction$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		var pathSuffix = "/credits/" + Reference;
		DeleteTx(this.apickli,pathSuffix,Reference,callback);
	});
	

	};
