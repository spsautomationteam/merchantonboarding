/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();
var prettyJson = require('prettyjson');
var Contenttype = "application/json";

var stepContext = {};

var createCreditTx = function( apickli, callback ) {
		var pathSuffix = "/credits";
		var url = apickli.domain + pathSuffix;
		var body = { ECommerce: { Amounts: { Total: 15.0 }, CardData: { Expiration: 1216, Number: 4111111111111111 } } };
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
        	callback();
		});
};


var getCreditLists = function( apickli, callback ) {
		var pathSuffix = "/credits";
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
        apickli.headers['merchantId'] =apickli.scenarioVariables.merchantId;
        apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getCredit = function( apickli, Reference, callback ) {
		var pathSuffix = "/credits/" + Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log(response.body);
			}
        	callback();
		});
};


module.exports = function () { 

	this.When(/^I make a credit transaction$/, function (callback) {
		createCreditTx(this.apickli, callback);
	});
	
	this.When(/^I perform get credit transactions$/, function ( callback) {
      	//this.apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
		getCreditLists(this.apickli, callback);

	});
	
	this.When(/^I get that same credit$/, function (callback) {
        var Reference = this.apickli.scenarioVariables.Reference;
		getCredit(this.apickli, Reference, callback);
	});
	
	
};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
