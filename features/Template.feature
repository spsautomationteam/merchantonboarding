@charges_GET
Feature: Charges
	As an API consumer
	I want to query Ping requests
	So that I know they have been processed
		
	@template
    Scenario: Verify the API proxy is responding
        Given I set Authorization header to Bearer SPS-DEV-CORE:TEST
        And I set SPS-User header to 403652
		And I set Content-Type header to "application/json"
		#And I set Accept header to "application/json"

		When I GET /Templates/AdvanceFundingProcessors
		#When I GET /ping
	    Then response code should be 200
        And response body should contain "title":"AdvanceMe"

